//
//  HomeBootCollectionViewCell.swift
//  Boot
//
//  Created by mahroUS on 27/07/2565 BE.
//

import UIKit

class HomeBootCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ordersTableView: UITableView!
    @IBOutlet weak var heightTableview: NSLayoutConstraint!
    let screenSize = UIScreen.main.bounds
    var orderList: [String] = ["Book A Ride",
                               "Transferring Package",
                               "Moving Goods",
                               "Go To Specific city",
                               "Call A Bubuzy Support Team"
    ]

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        // Initialization code
    }

}
extension HomeBootCollectionViewCell {
   
    private func setupView() {
        
        ordersTableView.delegate = self
        ordersTableView.dataSource = self
        ordersTableView.register(cellType: HomeBootTableViewCell.self)
        ordersTableView.frame.size.width = screenSize.width
        ordersTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        ordersTableView.backgroundColor = .clear
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        ordersTableView.layer.removeAllAnimations()
        
        heightTableview.constant = ordersTableView.contentSize.height
 
        UIView.animate(withDuration: 0.5) {
            self.layoutIfNeeded()
        }
    }

}

extension HomeBootCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderList.isEmpty {
            return 1
        } else {
            return orderList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: HomeBootTableViewCell.self, for: indexPath)
        cell.backgroundColor = .clear
        cell.containerView.layer.cornerRadius = 8
        switch indexPath.row {
        case 0 :
            cell.icon.image = #imageLiteral(resourceName: "Group 9")
            cell.titleLB.text = orderList[indexPath.row]
            cell.emergencyIcon.isHidden = true
        case 1 :
            cell.icon.image = #imageLiteral(resourceName: "box")
            cell.titleLB.text = orderList[indexPath.row]
            cell.emergencyIcon.isHidden = true
        case 2:
            cell.icon.image = #imageLiteral(resourceName: "Group 9685")
            cell.titleLB.text = orderList[indexPath.row]
            cell.emergencyIcon.isHidden = true
        case 3 :
            cell.icon.image = #imageLiteral(resourceName: "Group 9686")
            cell.titleLB.text = orderList[indexPath.row]
            cell.emergencyIcon.isHidden = true

        default :
            cell.icon.image = #imageLiteral(resourceName: "Group 9696")
            cell.titleLB.text = orderList[indexPath.row]
            cell.emergencyIcon.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

