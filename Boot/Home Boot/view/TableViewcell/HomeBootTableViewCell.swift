//
//  HomeBootTableViewCell.swift
//  Boot
//
//  Created by mahroUS on 27/07/2565 BE.
//

import UIKit

class HomeBootTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var emergencyIcon: UIImageView!
    @IBOutlet weak var widthCell: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
}
