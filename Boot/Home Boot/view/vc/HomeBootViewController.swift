//
//  HomeBootViewController.swift
//  Boot
//
//  Created by mahroUS on 27/07/2565 BE.
//

import UIKit
protocol HomeBootViewProtocol{
    
}
class HomeBootViewController: UIViewController {

    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var bootImage: UIImageView!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var containerOrdersView: UIView!
    @IBOutlet weak var containerVoiceView: UIView!
    @IBOutlet weak var voiceView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    private weak var displayLink: CADisplayLink?
    private var startTime: CFTimeInterval = 0
    private let shapeLayer: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 3
        return shapeLayer
    }()
   
    private let shapeLayer2: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 3
        return shapeLayer
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupCollectionView()
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        containerVoiceView.layer.addSublayer(shapeLayer)
        containerVoiceView.layer.addSublayer(shapeLayer2)
        startDisplayLink()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopDisplayLink()
    }
}
extension HomeBootViewController {

    private func setupUI() {
        voiceView.layer.cornerRadius = (voiceView.frame.size.width / 2)
        containerOrdersView.layer.cornerRadius = 10
        UIView.animate(withDuration: 1.0, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func setupCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.register(cellType: HomeBootCollectionViewCell.self)
        collectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        collectionView.layer.removeAllAnimations()
        
        heightCollectionView.constant = collectionView.contentSize.height
        print("heightCollectionView is:", heightCollectionView.constant)

        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }


    private func startDisplayLink() {
        startTime = CACurrentMediaTime()
        self.displayLink?.invalidate()
        let displayLink = CADisplayLink(target: self, selector:#selector(handleDisplayLink(_:)))
        displayLink.add(to: .main, forMode: .common)
        self.displayLink = displayLink
    }

    private func stopDisplayLink() {
        displayLink?.invalidate()
    }

    @objc func handleDisplayLink(_ displayLink: CADisplayLink) {
        let elapsed = CACurrentMediaTime() - startTime

        shapeLayer.path = wave(at: elapsed).0.cgPath
        shapeLayer2.path = wave(at: (elapsed - 10)).1.cgPath
        
    }


    private func wave(at elapsed: Double) -> (UIBezierPath, UIBezierPath) {
        let elapsed = CGFloat(elapsed)
        let centerY = containerVoiceView.bounds.midY
        let amplitude = 20 - abs(elapsed.remainder(dividingBy: 2)) * 15

        func f(_ x: CGFloat) -> CGFloat {
            return sin((x + elapsed) * 1 * .pi) * amplitude + centerY
        }

        let path = UIBezierPath()
        let path2 = UIBezierPath()
        let steps = Int(containerVoiceView.bounds.width / 20)

        path.move(to: CGPoint(x: 0, y: f(0)))
        for step in 1 ... steps {
            let x = CGFloat(step) / CGFloat(steps)
            path.addLine(to: CGPoint(x: x * containerVoiceView.bounds.width, y: f(x)))
        }

        path2.move(to: CGPoint(x: 0, y: f(0)))
        for step in 1 ... steps {
            let x = CGFloat(step) / CGFloat(steps)
            path2.addLine(to: CGPoint(x: x * containerVoiceView.bounds.width, y: f(x)))

        }

        return (path,path2)
    }


}
//
extension HomeBootViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(with: HomeBootCollectionViewCell.self, for: indexPath)
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width - 20, height: collectionView.frame.size.height)
    }
}

